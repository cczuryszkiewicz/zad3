---
author: Marcel Czuryszkiewicz
title: Wes Anderson
subtitle: Informacje o specyficznym reżyserze 
date: 8 listopada 2020
theme: Antibes
output: beamer_presentation
---

## Wes Anderson 

![wes_anderson](pics/wes.jpg)

## Wstęp

**Wes Anderson** - amerykański reżyser, scenarzysta, producent filmowy i aktor, znany z ekscentrycznego i niekonwencjonalnego stylu swoich autorskich filmów. Laureat wielu prestiżowych nagród, m.in. Srebrnego Niedźwiedzia – Grand Prix Jury na 64. MFF w Berlinie za film Grand Budapest Hotel (2014). Berlinale za Wyspę psów (2018). **Był sześciokrotnie nominowany do Oscara

## Życiorys

**Wes** Jest drugim z trzech synów Melvera Leonarda Andersona, właściciela firmy reklamowej w Houston, i Texas Ann Burroughs, archeolog i agentki nieruchomości. Ukończył szkołę średnią Westchester High School i prywatną szkołę St. John’s School w Houston, gdzie był autorem sztuki Rushmore, wystawianej na scenie Hoodwink Theatre. Studiował filozofię na University of Texas w Austin.

## Estetyka

Wes Anderson rozkochał w sobie publiczność nie tylko dzięki dziwacznym bohaterom. Od jego dopracowanych pod każdym kątem obrazów trudno oderwać wzrok. Reżyser jeździ po całym świecie, szukając inspiracji i odpowiednich miejsc do kręcenia filmów. Jego filmy mają wszystko, co pozwala je określić mianem stylowych. Bije z nich estetyka retro. Piękne sukienki, golfy i marynarki, pastelowe wnętrza budynków i wspaniale dobrana ścieżka dźwiękowa, na którą bardzo często składają się przeboje The Beatles, Davida Bowiegoczy też The Rolling Stones. Nie można też zapomnieć o muzyce Alexandre’a Desplata.



### Filmy:

- 2020: The French Dispatch
- 2018: Wyspa psów (Isle of Dogs)
- 2014: Grand Budapest Hotel (The Grand Budapest Hotel)
- 2012: Kochankowie z Księżyca. Moonrise Kingdom (Moonrise Kingdom)
- 2009: Fantastyczny pan Lis (Fantastic Mr. Fox)
- 2007: Pociąg do Darjeeling (The Darjeeling Limited)
- 2004: Podwodne życie ze Steve’em Zissou (The Life Aquatic with Steve Zissou)
- 2001: Genialny klan (The Royal Tenenbaums)
- 1998: Rushmore
- 1996: Trzech facetów z Teksasu (Bottle Rocket)

## Krótka lista nagród

|  ROK   |        festiwal         |            kategoria            |         za           |
|:------:|:-----------------------:|:-------------------------------:| :-------------------:|
|  2018  |Nagroda Akademii Filmowej|Najlepszy film animowany         |   wyspa psów         |
|  2014  |Nagroda Akademii Filmowej|Najlepszy film| 21 września 2016 |Grand Budapest Hotel  |
|  2014  |Nagroda Akademii Filmowej|Najlepsza reżyseria              |Grand Budapest Hotel  |
|  2012  |Nagroda Akademii Filmowej|Najlepszy scenariusz oryginalny  |Kochankowie z Księżyca|

**Wes był nominowany do wielu inncyh nagród**




